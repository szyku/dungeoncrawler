<?php

namespace AppBundle\Entity;

/**
 * Sessions
 *
 * This entity was created to underline session handling through database. This entity should only be read only.
 * All session storage writing should be handled by the application until you know what you are doing.
 */
class Sessions
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $data;

    /**
     * @var integer
     */
    private $time;

    /**
     * @var integer
     */
    private $lifetime;


    /**
     * Get id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get data
     *
     * @return string
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Get time
     *
     * @return integer
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * Get lifetime
     *
     * @return integer
     */
    public function getLifetime()
    {
        return $this->lifetime;
    }
}

