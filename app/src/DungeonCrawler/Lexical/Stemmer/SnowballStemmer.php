<?php
/**
 * Created by PhpStorm.
 * User: Szymon Szymański
 * Date: 23.04.2017
 * Time: 18:20
 */

namespace DungeonCrawler\Lexical\Stemmer;


use DungeonCrawler\Lexical\Stemmer\Exception\InvalidArgumentException;
use DungeonCrawler\Lexical\Stemmer\Exception\NotStringTypeException;
use DungeonCrawler\Lexical\Stemmer\Stemmer;
use Traversable;
use function Stringy\create as S;

class SnowballStemmer implements Stemmer
{
    private const ENCODING = 'UTF_8';
    private const LANG = 'english';

    private const SPACE = ' ';

    public function stem(string $word): string
    {
        $wrapped = S($word)->collapseWhitespace();
        if ($wrapped->contains(self::SPACE))
        {
            $tokens = explode(self::SPACE, (string)$wrapped);
            $stems = [];
            foreach ($tokens as $token) {
                $stems[] = (string)stemword($token, self::LANG, self::ENCODING);
            }

            return implode(self::SPACE, $stems);
        }

        return (string)stemword($word, self::LANG, self::ENCODING);
    }

    /**
     * @param array ...$words
     * @return array
     * @throws NotStringTypeException When one of arguments is not string type
     */
    public function stemMany(...$words): array
    {
        return $this->internalCollectionProcessing($words);

    }

    /**
     * @param array|Traversable $collection
     * @return array
     * @throws NotStringTypeException When one of arguments is not string type
     */
    public function stemCollection($collection): array
    {
        return $this->internalCollectionProcessing($collection);
    }

    private function assertCollection($collection)
    {
        if (!is_array($collection) && !$collection instanceof \Traversable) {
            throw new InvalidArgumentException("Invalid argument type has been passed. Use arrays or Traversable collections excluding stdObjects.");
        }

        foreach ($collection as $idx => $word) {
            if (!is_string($word)) {
                $position = 1 + $idx;
                throw new NotStringTypeException("The value can be found at position $position", $position);
            }
        }
    }

    /**
     * @param Traversable $collection
     * @param $words
     * @return array
     */
    private function internalCollectionProcessing($words): array
    {
        $this->assertCollection($words);

        $stemmedWords = [];
        foreach ($words as $word) {
            $stemmedWords[] = $this->stem($word);
        }

        return $stemmedWords;
    }
}
