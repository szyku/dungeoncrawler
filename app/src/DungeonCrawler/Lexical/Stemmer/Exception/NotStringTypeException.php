<?php
/**
 * Created by PhpStorm.
 * User: Szymon Szymański
 * Date: 23.04.2017
 * Time: 18:53
 */

namespace DungeonCrawler\Lexical\Stemmer\Exception;


use DungeonCrawler\Lexical\Stemmer\Exception\LexicalException;

class NotStringTypeException extends \InvalidArgumentException implements LexicalException
{
    /** @var int */
    private $argumentNumber;

    /**
     * NotStringTypeException constructor.
     * @param string $message
     * @param int $argumentNumber
     */
    public function __construct(string $message, int $argumentNumber)
    {
        parent::__construct($message, 0, null);
        $this->argumentNumber = $argumentNumber;
    }

}
