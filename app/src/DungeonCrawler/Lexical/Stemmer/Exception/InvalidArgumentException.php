<?php
/**
 * Created by PhpStorm.
 * User: Szymon Szymański
 * Date: 23.04.2017
 * Time: 20:54
 */

namespace DungeonCrawler\Lexical\Stemmer\Exception;


use InvalidArgumentException as SplInvalidArgumentException;

class InvalidArgumentException extends SplInvalidArgumentException implements LexicalException
{

}
