<?php
/**
 * Created by PhpStorm.
 * User: Szymon Szymański
 * Date: 23.04.2017
 * Time: 18:19
 */

namespace DungeonCrawler\Lexical\Stemmer;


use DungeonCrawler\Lexical\Stemmer\Exception\InvalidArgumentException;
use DungeonCrawler\Lexical\Stemmer\Exception\NotStringTypeException;
use Traversable;

interface Stemmer
{
    public function stem(string $word): string;

    /**
     * @param array ...$words
     * @return array
     * @throws NotStringTypeException When one of arguments is not string type
     */
    public function stemMany(...$words): array;

    /**
     * @param array|Traversable $collection
     * @return array
     * @throws NotStringTypeException When one of arguments is not string type
     * @throws InvalidArgumentException When $collection is neither array nor Traversable.
     */
    public function stemCollection($collection): array;
}
