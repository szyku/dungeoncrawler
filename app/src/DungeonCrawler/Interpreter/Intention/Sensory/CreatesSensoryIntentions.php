<?php
/**
 * Created by PhpStorm.
 * User: Szymon Szymański
 * Date: 21.04.2017
 * Time: 23:43
 */

namespace DungeonCrawler\Interpreter\Intention\Sensory;


use DungeonCrawler\Interpreter\Intention\Exception\IntentionStateException;

interface CreatesSensoryIntentions
{
    /**
     * @param string $input
     * @return Sensory
     * @throws IntentionStateException
     */
    public function create(string $input): Sensory;

    /**
     * @param string $input
     * @return bool
     */
    public function canCreate(string $input): bool;
}
