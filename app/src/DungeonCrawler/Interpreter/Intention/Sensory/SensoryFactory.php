<?php
/**
 * Created by PhpStorm.
 * User: Szymon Szymański
 * Date: 21.04.2017
 * Time: 23:43
 */

namespace DungeonCrawler\Interpreter\Intention\Sensory;

use DungeonCrawler\Interpreter\Intention\Exception\IntentionStateException;
use DungeonCrawler\Interpreter\Intention\IntentionVocabulary as Vocab;
use Stringy\Stringy;
use function Stringy\create as S;

class SensoryFactory implements CreatesSensoryIntentions
{
    private const VOCAB_CLASS_MAP = [
        Vocab::SENSORY_TOUCH       => Touch::class,
        Vocab::SENSORY_TASTE       => Taste::class,
        Vocab::SENSORY_SMELL       => Smell::class,
        Vocab::SENSORY_OBSERVATION => Observation::class,
        Vocab::SENSORY_HEARING     => Hearing::class,
    ];

    public function create(string $input): Sensory
    {
        $normalized = S($input);
        $sensoryType = $this->findSensoryType($normalized);
        if (null === $sensoryType || !in_array($sensoryType, self::VOCAB_CLASS_MAP)) {
            throw new IntentionStateException("Couldn't create sensory intention because the type could not be recognized.");
        }

        $className = self::VOCAB_CLASS_MAP[$sensoryType];
        $ref = new \ReflectionClass($sensoryType);
        /** @var Sensory $instance */
        $instance = $ref->newInstance();
        if (!($input instanceof Sensory)) {
            throw new IntentionStateException("The factory created something unexpected. The object should be Sensory type, but it is not.");
        }

        return $instance;
    }

    public function canCreate(string $input): bool
    {
        $normalized = S($input);

        return null !== $this->findSensoryType($normalized);
    }

    /**
     * @param $normalized
     * @return bool
     */
    private function findSensoryType(Stringy $normalized): ?string
    {
        foreach (Vocab::VERBS_SENSORY as $sensoryType => $verbs) {
            if ($normalized->containsAny($verbs)) {
                return $sensoryType;
            }
        }

        return null;
    }
}
