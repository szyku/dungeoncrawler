<?php
/**
 * Created by PhpStorm.
 * User: Szymon Szymański
 * Date: 21.04.2017
 * Time: 21:08
 */

namespace DungeonCrawler\Interpreter\Intention;


final class Gibberish extends Intention
{
    public function __construct(string $phrase)
    {
        parent::__construct($phrase, null);
    }

}
