<?php
/**
 * Created by PhpStorm.
 * User: Szymon Szymański
 * Date: 21.04.2017
 * Time: 23:37
 */

namespace DungeonCrawler\Interpreter\Intention;


class IntentionVocabulary
{
    public const SENSORY_HEARING = 'hearing';
    public const SENSORY_OBSERVATION = 'observation';
    public const SENSORY_SMELL = 'smell';
    public const SENSORY_TASTE = 'taste';
    public const SENSORY_TOUCH = 'touch';

    public const VERBS_MOVEMENT = [
        'run',
        'jump',
        'sneak',
        'walk',
        'turn around',
        'crawl',
        'back away',
    ];

    public const VERBS_ACTON = [
        'open',
        'close',
        'turn off',
        'turn on',
        'throw away',
        'push',
        'pull',
        'kill',
        'commit suicide',
    ];

    public const VERBS_SENSORY = [
        self::SENSORY_HEARING     => ['listen',],
        self::SENSORY_OBSERVATION => ['look', 'gaze'],
        self::SENSORY_SMELL       => ['smell', 'sniff'],
        self::SENSORY_TASTE       => ['taste'],
        self::SENSORY_TOUCH       => ['feel', 'touch'],
    ];
}
