<?php
namespace DungeonCrawler\Interpreter\Intention\Exception;

use Throwable;

/**
 * Created by PhpStorm.
 * User: Szymon Szymański
 * Date: 21.04.2017
 * Time: 23:19
 */
class IntentionStateException extends \LogicException
{
    public function __construct($message = "", Throwable $previous = null)
    {
        parent::__construct($message, 0, $previous);
    }

}
