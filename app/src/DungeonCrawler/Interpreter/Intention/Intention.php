<?php
/**
 * Created by PhpStorm.
 * User: Szymon Szymański
 * Date: 21.04.2017
 * Time: 20:50
 */

namespace DungeonCrawler\Interpreter\Intention;


use DungeonCrawler\Interpreter\Intention\Exception\IntentionStateException;

/**
 * Class Intention
 *
 * Represents a player's intention.
 *
 * @package DungeonCrawler\Interpreter\Intention
 */
abstract class Intention
{
    /** @var string */
    private $intentionPhrase;

    /** @var string */
    private $intentionObject = null;

    /**
     * Intention constructor.
     * @param string $intentionPhrase
     * @param string $intentionObject
     */
    public function __construct(string $intentionPhrase, string $intentionObject = null)
    {
        $this->intentionPhrase = $intentionPhrase;
        $this->intentionObject = $intentionObject;
    }

    public function intentionPhrase(): string
    {
        return $this->intentionPhrase;
    }

    public function hasIntentionObject(): bool
    {
        return null !== $this->intentionObject;
    }

    public function intentionObject(): string
    {
        if (null === $this->intentionObject) {
            throw new IntentionStateException("Tried to retrieve intention object, but it is not set.");
        }

        return $this->intentionObject;
    }

    function __toString()
    {
        $output = $this->intentionPhrase;
        if ($this->hasIntentionObject()) {
            $output .= ' ' . $this->intentionObject;
        }

        return $output;
    }


}
