<?php
/**
 * Created by PhpStorm.
 * User: Szymon Szymański
 * Date: 21.04.2017
 * Time: 20:34
 */

namespace DungeonCrawler\Interpreter;


use DungeonCrawler\Interpreter\Intention\Intention;

interface InterpretsPlayersInput
{
    public function interpret(string $input): Intention;
}
