<?php

namespace DungeonCrawler\Interpreter;

use DungeonCrawler\Interpreter\Intention\Action;
use DungeonCrawler\Interpreter\Intention\Gibberish;
use DungeonCrawler\Interpreter\Intention\Intention;
use DungeonCrawler\Interpreter\Intention\Movement;
use DungeonCrawler\Interpreter\Intention\IntentionVocabulary as Vocab;

use DungeonCrawler\Interpreter\Intention\Sensory\CreatesSensoryIntentions;
use function Stringy\create as S;

/**
 * Class Interpreter
 *
 * Categorizes words into intentions.
 *
 * @package DungeonCrawler\Interpreter
 */
class Interpreter implements InterpretsPlayersInput
{
    /** @var  CreatesSensoryIntentions */
    private $sensoryFactory;

    public function __construct(CreatesSensoryIntentions $createsSensoryIntentions)
    {
        $this->sensoryFactory = $createsSensoryIntentions;
    }

    public function interpret(string $input): Intention
    {
        $normalized = S($input)->collapseWhitespace()->toLowerCase();
        $normalizedRawStr = (string)$normalized;

        if ($normalized->containsAny(Vocab::VERBS_MOVEMENT)) {
            return new Movement($normalizedRawStr);
        }

        if ($normalized->containsAny(Vocab::VERBS_ACTON)) {
            return new Action($normalizedRawStr);
        }

        if ($this->sensoryFactory->canCreate((string)$normalized)){
            return $this->sensoryFactory->create((string)$normalized);
        }

        return new Gibberish((string) $normalized);
    }
}
