<?php

namespace spec\DungeonCrawler\Lexical\Stemmer;

use DungeonCrawler\Lexical\Stemmer\Exception\InvalidArgumentException;
use DungeonCrawler\Lexical\Stemmer\Exception\NotStringTypeException;
use DungeonCrawler\Lexical\Stemmer\SnowballStemmer;
use DungeonCrawler\Lexical\Stemmer\Stemmer;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class SnowballStemmerSpec extends ObjectBehavior
{
    private const SRC = 'source';
    private const EXPECT = 'expectation';

    function it_is_initializable()
    {
        $this->shouldHaveType(SnowballStemmer::class);
        $this->shouldImplement(Stemmer::class);
    }

    public function it_stems_given_string_to_a_root()
    {
        foreach ($this->testWords() as $testPair) {
            $this->stem($testPair[self::SRC])->shouldReturn($testPair[self::EXPECT]);
        }
    }

    public function it_stems_given_phrases()
    {
        $this->stem("testing tester helps out with healing bisons calves")->shouldEqual('test tester help out with heal bison calv');
    }

    public function it_stems_many_items()
    {
        $result = $this->stemMany('testing', 'tester', 'helps');
        $result->shouldBeArray();
        $result[0]->shouldBe('test');
        $result[1]->shouldBe('tester');
        $result[2]->shouldBe('help');
    }

    public function it_stems_collections()
    {
        $splCollection = new \SplQueue();
        $splCollection->push('testing');
        $splCollection->push('tester');
        $splCollection->push('helps');

        $result = $this->stemCollection($splCollection);

        $result->shouldBeArray();
        $result[0]->shouldBe('test');
        $result[1]->shouldBe('tester');
        $result[2]->shouldBe('help');

        $splOtherCollection = new \SplStack();
        $splOtherCollection->push('tested');
        $splOtherCollection->push('physics');
        $splOtherCollection->push('maths');
    }

    public function its_rejects_incorrectly_provided_data()
    {
        $this->shouldThrow(NotStringTypeException::class)->duringStemMany('correct', 123, 'correct');

        $splCollection = new \SplFixedArray(2);
        $splCollection[0] = 'correct';
        $splCollection[1] = 1.23456;

        $this->shouldThrow(NotStringTypeException::class)->duringStemCollection($splCollection);

        $notCorrectTraversable = new \stdClass();
        $notCorrectTraversable->word = 'correct';

        $this->shouldThrow(InvalidArgumentException::class)->duringStemCollection($notCorrectTraversable);
    }

    private function testWords(): array
    {
        return [
            [self::SRC => 'test', self::EXPECT => 'test'],
            [self::SRC => 'testing', self::EXPECT => 'test'],
            [self::SRC => 'tester', self::EXPECT => 'tester'],
            [self::SRC => 'tested', self::EXPECT => 'test'],
            [self::SRC => 'built', self::EXPECT => 'built'],
            [self::SRC => 'build', self::EXPECT => 'build'],
            [self::SRC => 'helps', self::EXPECT => 'help'],
            [self::SRC => 'dogs', self::EXPECT => 'dog'],
            [self::SRC => 'mouse', self::EXPECT => 'mous'],
            [self::SRC => 'mice', self::EXPECT => 'mice'],
            [self::SRC => 'goose', self::EXPECT => 'goos'],
            [self::SRC => 'geese', self::EXPECT => 'gees'],
            [self::SRC => 'translating', self::EXPECT => 'translat'],
            [self::SRC => 'mice', self::EXPECT => 'mice'],
        ];
    }
}
