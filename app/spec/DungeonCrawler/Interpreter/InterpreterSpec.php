<?php

namespace spec\DungeonCrawler\Interpreter;

use DungeonCrawler\Interpreter\Intention\Action;
use DungeonCrawler\Interpreter\Intention\Gibberish;
use DungeonCrawler\Interpreter\Intention\Intention;
use DungeonCrawler\Interpreter\Intention\Movement;
use DungeonCrawler\Interpreter\Intention\Sensory\CreatesSensoryIntentions;
use DungeonCrawler\Interpreter\Intention\Sensory\Hearing;
use DungeonCrawler\Interpreter\Intention\Sensory\Observation;
use DungeonCrawler\Interpreter\Intention\Sensory\Sensory;
use DungeonCrawler\Interpreter\Intention\Sensory\Smell;
use DungeonCrawler\Interpreter\Intention\Sensory\Taste;
use DungeonCrawler\Interpreter\Intention\Sensory\Touch;
use DungeonCrawler\Interpreter\Interpreter;
use DungeonCrawler\Interpreter\InterpretsPlayersInput;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class InterpreterSpec extends ObjectBehavior
{
    const PHRASE = 'phrase';

    const EXPECT = 'expectation';

    function let(CreatesSensoryIntentions $createsSensoryIntentions)
    {
        $this->beConstructedWith($createsSensoryIntentions);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(Interpreter::class);
        $this->shouldImplement(InterpretsPlayersInput::class);
    }

    public function it_accepts_raw_strings_to_interpret(CreatesSensoryIntentions $createsSensoryIntentions)
    {
        $createsSensoryIntentions->canCreate(Argument::any())->willReturn(false);
        $this->interpret("Some string");
    }

    public function it_returns_intention_as_a_result_of_interpretation(CreatesSensoryIntentions $createsSensoryIntentions)
    {
        $createsSensoryIntentions->canCreate(Argument::any())->willReturn(false);
        $result = $this->interpret("Some string");
        $result->shouldBeAnInstanceOf(Intention::class);
    }

    public function it_interprets_incomprehensible_input_as_gibberish(CreatesSensoryIntentions $createsSensoryIntentions)
    {
        $createsSensoryIntentions->canCreate(Argument::any())->willReturn(false);
        $result = $this->interpret("a gugu gaga?");
        $result->shouldBeAnInstanceOf(Gibberish::class);
        $result->shouldBeAnInstanceOf(Intention::class);
    }

    public function it_interprets_movement_descriptions_as_movement_intention(CreatesSensoryIntentions $createsSensoryIntentions)
    {
        foreach ($this->getMovementDescriptions() as $description) {
            $createsSensoryIntentions->canCreate(Argument::any())->willReturn(false);
            $this->interpret($description)->shouldBeAnInstanceOf(Movement::class);
        }
    }

    public function it_interprets_interactions_as_action_intentions()
    {
        foreach ($this->getActionDescriptions() as $description) {
            $this->interpret($description)->shouldBeAnInstanceOf(Action::class);
        }
    }

    public function it_interprets_environmental_probing_as_sensory_intentions(CreatesSensoryIntentions $createsSensoryIntentions)
    {
        foreach ($this->getSensoryDescriptionsWithExpectations() as $row) {
            $description = $row[self::PHRASE];
            $expectation = $row[self::EXPECT];
            $createsSensoryIntentions->canCreate($description)->willReturn(true);

            $ref = new \ReflectionClass($expectation);
            $class = $ref->newInstance($description);
            $createsSensoryIntentions->create($description)->willReturn($class);

            $this->interpret($description)->shouldBeAnInstanceOf($expectation);
        }
    }

    private function getMovementDescriptions(): array
    {
        return [
            'run forward',
            'jump over the lazy dog',
            'sneak pass the hideous troll',
            'walk forward like a champion',
            'walk towards the greasy monkey',
            'turn around like a pansy',
            'crawl under the bed',
            'back away',
        ];
    }

    private function getActionDescriptions(): array
    {
        return [
            'open the door',
            'close the window',
            'turn off the light',
            'turn on the water pump',
            'throw away the unloved child',
            'push the rock',
            'pull the trigger',
            'kill the empathy',
            'commit suicide',
        ];
    }

    private function getSensoryDescriptionsWithExpectations(): array
    {
        return [
            [self::PHRASE => 'touch the wall', self::EXPECT => Touch::class],
            [self::PHRASE => 'look at the vermin', self::EXPECT => Observation::class],
            [self::PHRASE => 'listen carefully', self::EXPECT => Hearing::class],
            [self::PHRASE => 'taste the knife', self::EXPECT => Taste::class],
            [self::PHRASE => 'gaze into the darkness', self::EXPECT => Observation::class],
            [self::PHRASE => 'smell the bun', self::EXPECT => Smell::class],
            [self::PHRASE => 'sniff for trouble', self::EXPECT => Smell::class],
            [self::PHRASE => 'feel the water', self::EXPECT => Touch::class],
        ];
    }

}
