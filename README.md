# Text Dungeon Crawler #

The aim of this project is to provide a text-based dungon-like crawler game.
Whilst players from various regions will approach the challange in various ways,
the creators are mostly interested _how the players tries to overcome the dungeon_, 
that is to say, what one will type and what does one expect to work.

## DevOps ##

A docker setup has been prepared for the developers.

Currently, **there is no production** setup prepared.

Whilst reading the examples bellow, have in mind that `ARGS` just appends all options and arguments to the end of the instruction.
So, to illustrate, the command `composer_update` has `ARGS` to pass any additional arguments. By typing `composer_update ARGS="phpunit/phpunit"`
one executes a command like `composer require phpunit/phpunit`. If you put `--dev` before the package name, you'll add the dev option.
`ARGS` usually are optional.

## Tech Stack ##

+ PHP-fpm 7.1
+ python 3.6.0
+ nginx

### Highlighted libs ###

+ For assertions [beberlei/assert](https://github.com/beberlei/assert)
+ Enum substitution [marc-mabe/php-enum](https://github.com/marc-mabe/php-enum)
+ Better string manipulation - [danielstjules/stringy](https://github.com/danielstjules/Stringy)

## How to build this project ##

### Prerequisites ###

Docker at a version that supports docker-compose. This guide was compiled with:

+ Docker version 17.03.0-ce, build 60ccb22
+ docker-compose version 1.11.2, build dfed245

### Build steps ###

1. `docker-compose up -d`
2. `make`
3. ???
4. profit

## Dependencies ##

### Composer ###

1. `make composer` - install dependencies from lock file (in no interaction mode).
2. `make composer_update` - for updating the whole project.
3. `make composer_update ARGS="some/package"` - for updating certain package.
4. `make composer_require ARGS="--dev teh/package` - for installing new packages.
5. `make composer_remove ARGS="obsolete/package"` - for removing a package.

### Stemmer ###

There's an additional module installed which was build earlier on a local machine. If you wish to build it yourself, you can find the sources under [here](https://github.com/hthetiot/php-stemmer). The official PECL package is outdated and doesn't work well... suprising.

This stemmer dependency implements algorithms from [Snowball stem](http://snowballstem.org/). It's recommended to check the demo section. 

### Symfony console shortcuts ###

* `make sf` - mapped to `bin/console`.
* `make migrate` - mapped to `bin/console d:m:m` (if installed)
* `make enter_app` - go directly to the application container.

## Tests ##

For behavioral testing phpspec is used. For everything else phpunit.

### Running & creating tests through docker ###

### Phpspec ###

+ `make spec` - for running specifications.
+ `make spec ARGS="Class/Path"` - for running a certain specification.
+ `make spec_desc ARGS="Your/Fully/Qualified/Class/Path"` - to create a specification.
+ `make spec_desc ARGS="'Your\Fully\Qualified\Class\Path'"` - the same as above, but using single quotation marks.

Whilst creating a specification, **be sure to use slashes (/), not backslashes (\\) to separate namespace fragments** or single quotation marks (') to prevent phpspec from wrongly interpreting the namespace. 

### Phpunit ###

+ `make unit` - for running all unit tests.
+ `make unit ARGS="--testsuite=lawsuit"` - for specifing additional options or passing arguments to phpunit.

### Running all tests at once ###

`make tests` - this does not take arguments (`ARGS`)

## Troubleshooting ##

### Bin folder is not populated with vendor executables ###

For unknown reason, dockerized composer does not populate the app/bin folder with app/vendor executables. To solve this, enter the container and run composer manually. This is probably an error on the devops side, not the image.

### Database permissions ###

If you own a Mac (or at least run this project on one), you might get an error as follows:

```chown: changing ownership of '/var/lib/mysql/': Operation not permitted```

This occurs because OS X does not map permissions from host to containers. Also, the lib providers reported that they do not support a spectrum of volume drivers. 

There are two quick workarounds:
+ Switch to Linux or VM with Linux.
+ Remove the volume from docker-compose and deal with it by dumping and restoring databases.

### app/bin does not have other binaries than console and symfony_requirements ###

This happens sometimes when you use `make composer` and you have already something in vendor directory.

To quickly solve this, enter the container (`make enter_app`) and run `composer install` from the inside. However,
to be consistent, removing vendor and running `make composer` will also solve this.