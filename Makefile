ROOT := $(shell pwd)
APP_DIR = $(ROOT)/app
DEF_SHELL =sh -c
COMPOSER_IMG = composer/composer:alpine
APP_CONTAINER = tc_php-fpm 
ARGS ?=

build: composer migrate_no_interaction

.PHONY: composer composer_update composer_require composer_remove sf migrate spec spec_desc spec unit tests migrate_no_interaction enter_app
composer:
	@docker run --rm -v $(APP_DIR):/app $(COMPOSER_IMG) install --no-interaction
composer_update:
	@docker run --rm -v $(APP_DIR):/app $(COMPOSER_IMG) update $(ARGS)
composer_require:
	@docker run --rm -v $(APP_DIR):/app $(COMPOSER_IMG) require $(ARGS)
composer_remove:
	@docker run --rm -v $(APP_DIR):/app $(COMPOSER_IMG) remove $(ARGS)
spec_desc:
	@docker exec -it $(APP_CONTAINER) $(DEF_SHELL) "bin/phpspec desc $(ARGS)"	
spec:
	@docker exec -it $(APP_CONTAINER) $(DEF_SHELL) "bin/phpspec run $(ARGS)"
unit:
	@docker exec -it $(APP_CONTAINER) $(DEF_SHELL) "bin/phpunit $(ARGS)"
sf:
	@docker exec -it $(APP_CONTAINER) $(DEF_SHELL) "bin/console $(ARGS)"
migrate:
	@docker exec -it $(APP_CONTAINER) $(DEF_SHELL) "bin/console d:m:m"
migrate_no_interaction:
	@docker exec -it $(APP_CONTAINER) $(DEF_SHELL) "bin/console d:m:m -n"
tests: spec unit
enter_app:
	@docker exec -it $(APP_CONTAINER) bash
